HW_Timer
===

An Arduino library wrapping the use of the 4 ESP32 HW timers into easy to use function calls.    

Why did I create this library    
When I started to program on the ESP32 there was no Ticker library available as I was used to from ESP8266 and Arduino. So I decided to write some code that makes it easy for me to use the ESP32's hardware timers.    

This library is only on for the ESP32.    

Change Log:
--------
- 2018-04-22: Created from some code stubs that I used before    
Features
--------
  - Easy use of the 4 HW timers
  - Keeps track which of the timers are already in use
  - User defined callbacks

Functions
-----
_**`hw_timer_t *startTimer (uint64_t usTriggerTime, callback_t callback, bool repeat);`**_    
- Allocate and start a timer    
- Return NULL if no timer is available, calling code must check the return value
- `usTriggerTime` - time for timer in **micro seconds**    
- `callback` - pointer to the users callback function     
- `repeat` - true for a repeating timer, false for a single shot timer     
- `RETURN` - hw_timer_t* to the timer (required to stop or repeat the timer) or NULL if no timer was available

_**`hw_timer_t *startTimerMsec (uint64_t msTriggerTime, callback_t callback, bool repeat);`**_    
- Allocate and start a timer    
- Return NULL if no timer is available, calling code must check the return value
- `msTriggerTime` - time for timer in **milli seconds**    
- `callback` - pointer to the users callback function     
- `repeat` - true for a repeating timer, false for a single shot timer     
- `RETURN` - hw_timer_t* to the timer (required to stop or repeat the timer) or NULL if no timer was available

_**`hw_timer_t *startTimerSec (uint64_t sTriggerTime, callback_t callback, bool repeat);`**_    
- Allocate and start a timer    
- Return NULL if no timer is available, calling code must check the return value
- `sTriggerTime` - time for timer in **seconds**    
- `callback` - pointer to the users callback function     
- `repeat` - true for a repeating timer, false for a single shot timer     
- `RETURN` - hw_timer_t* to the timer (required to stop or repeat the timer) or NULL if no timer was available

_**`void restartTimer(hw_timer_t *timerToRestart);`**_    
- Reset a repeating timer and start it again
- Restart a single-shot timer
- `timerToRestart` - pointer to timer returned from `startTimer()`        

_**`void stopTimer(hw_timer_t *timerToStop);`**_    
- Stop a running timer     
- `timerToRestart` - pointer to timer returned from `startTimer()`        

_**`void stopAllTimers();`**_    
- Stops all running timers     

Usage
-----
See [examples](https://github.com/beegee-tokyo/ESP32-HW_timer/blob/master/examples). For more information checkout [Using the HW timers of the ESP32](https://desire.giesecke.tk/index.php/2018/04/22/using-the-hw-timers-of-the-esp32).    

Installation
------------
Copy hw_timer.h and hw_timer.cpp into your project folder    
Include hw_timer.h into your sketch    
