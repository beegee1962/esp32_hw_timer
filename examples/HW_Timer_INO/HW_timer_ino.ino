/**
 * Example code for HW timer wrapper
 * Read details on https://desire.giesecke.tk/index.php/2018/04/22/using-the-hw-timers-of-the-esp32
 * Check README.md for usage
 * Code released under GNU GENERAL PUBLIC LICENSE Version 3
 */
#include <Arduino.h>
#include "hw_timer.h"

// HW timer structures
hw_timer_t *myTimer0;
hw_timer_t *myTimer1;
hw_timer_t *myTimer2;
hw_timer_t *myTimer3;
hw_timer_t *myTimer4;

// Flags for timer callbacks
volatile bool timer0Triggered = false;
volatile bool timer1Triggered = false;
volatile bool timer2Triggered = false;
volatile bool timer3Triggered = false;

void timer0cb () {
	timer0Triggered = true;
}

void timer1cb () {
	timer1Triggered = true;
}

void timer2cb () {
	timer2Triggered = true;
}

void timer3cb () {
	timer3Triggered = true;
}

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("HW timer wrapper example");

	// Start first timer repeating with 5 seconds time
	if ((myTimer0 = initTimerSec((uint64_t)5, timer0cb, true)) == NULL) {
		Serial.println("Could not start first timer");
	} else {
		Serial.println("Started first timer at " + String(millis()));
	}
	// Start second timer repeating with 8 seconds time
	if ((myTimer1 = initTimerSec((uint64_t)8, timer1cb, true)) == NULL) {
		Serial.println("Could not start second timer");
	} else {
		Serial.println("Started second timer at " + String(millis()));
	}
	// Start third timer repeating with 11 seconds time
	if ((myTimer2 = initTimerSec((uint64_t)11, timer2cb, true)) == NULL) {
		Serial.println("Could not start third timer");
	} else {
		Serial.println("Started third timer at " + String(millis()));
	}
	// Start forth timer single shot with 15 seconds time
	if ((myTimer3 = initTimerSec((uint64_t)15, timer3cb, false)) == NULL) {
		Serial.println("Could not start forth timer");
	} else {
		Serial.println("Started forth timer at " + String(millis()));
	}
	// Just for checking, try to start a fifth timer
	// This will always fail as there are only 4 HW timers
	// Start fifth timer repeating with 15 seconds time
	if ((myTimer4 = initTimerSec(15, timer3cb, true)) == NULL) {
		Serial.println("Could not start fifth timer");
	} else {
		Serial.println("Started fifth timer at " + String(millis()));
	}

	if (myTimer0 != NULL) {
		startTimer(myTimer0);
	}
	if (myTimer1 != NULL) {
		startTimer(myTimer1);
	}
	if (myTimer2 != NULL) {
		startTimer(myTimer2);
	}
	if (myTimer3 != NULL) {
		startTimer(myTimer3);
	}
}

void loop()
{
	if (timer0Triggered) {
		Serial.println("First timer was triggered at " + String(millis()));
		timer0Triggered = false;
	}
	if (timer1Triggered) {
		Serial.println("Second timer was triggered at " + String(millis()));
		timer1Triggered = false;
	}
	if (timer2Triggered) {
		Serial.println("Third timer was triggered at " + String(millis()));
		timer2Triggered = false;
	}
	if (timer3Triggered) {
		Serial.println("Forth timer was triggered at " + String(millis()));
		Serial.println("Timer was single-shot so this message comes only once!");
		timer3Triggered = false;
	}
}

